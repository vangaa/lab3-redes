# BlackJack
---

## Compilación

Ubíquese en la carpeta del proyecto y ejecute el comando `make`, el cual 
compilará el programa del servidor y el del cliente

    $ cd /ubicacion/del/proyecto/
    $ make

## Ejecución

Una vez creados ambos programas debe seguir las siguientes instrucciones:

Ejecute el programa `servidor` en la terminal. Este programa inicia la conexión 
con un puerto por defecto.

    $ ./servidor

Cuando el servidor esté en ejecución, se necesita abrir una segunda terminal y 
abrir el programa `cliente`, el cual se conectará al programa `servidor` para 
empezar el juego.

    $ ./cliente

El programa `cliente` se conectará al servidor usando el mismo puerto, y se le 
pedirá que apueste (indicación de que el juego ha empezado).


## El juego

Sobre el juego no hay mucho que decir. La mayoría de las instrucciones de como 
interactuar con el juego están explícitas o se explican dentro del programa.

Cuando quiera salir del juego, solo necesita hacer una apuesta de 0 (lo que 
quiere decir que no desea competir en esa ronda) lo que finalizará tanto el 
programa `cliente` como el `servidor`.

El juego como tal se realiza por completo en el cliente, el servidor solo se 
encarga de imprimir mensajes de debug, como el envío de cartas o aviso de 
mensajes recibidos/leídos.

## Funcionalidades añadidas

- [ ] Sistemas de apuestas (con doblar apuestas, separar apuestas, etc).
- [ ] Mesa de blackjack completa, es decir, posibilidad de funcionar desde 1 a 7 jugadores, conectándose en cualquier momento del juego.
- [X] Barajas de naipes limitadas, es decir, a medida que van quedando cartas fuera de juego, estas salgan del mazo.


