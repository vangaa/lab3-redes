#include "error_output.hpp"

#include <cstdlib>
#include <iostream>

void error(int ecode, const char *msg)
{
	perror(msg);
	exit(ecode);
}
