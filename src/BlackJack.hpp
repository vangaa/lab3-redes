#ifndef _BLACKJACK_H_
#define _BLACKJACK_H_

#include "Jugador.hpp"


class BlackJack
{
	private:
		int socket_fd;
		struct sockaddr_in socket_dir;

	public:
		/**
		 * Envia el pago al jugador. Este pago no es fijo, y depende del monto 
		 * de la apuesta que haya hecho el jugador y si sacó o no BlackJack (21 
		 * puntos con las 2 primeras cartas recibidas)
		 *
		 * @param mult  Es el multiplicador de la apuesta
		 * @param ply   Es el jugador al que se le paga
		 *
		 * @return 0 si se envió el pago correctamente, y un valor negativo en 
		 * otro caso
		 */
		int enviar_pago(float mult, Jugador& ply);
		/**
		 * Recibe un comando enviado por un jugador.
		 *
		 * @param ply es el jugador que mandó el comando
		 *
		 * @return Comando::NONE si hubo problemas en la lectura; de caso 
		 * contrario devuelve el comando enviado por «ply»
		 */
		static Comando recibir_decision(Jugador& ply);
		/**
		 * Recibe la apuesta de un jugador. Setea ademas el atributo «bet» 
		 * dentro del jugador "ply" con el valor recibido.
		 *
		 * @param ply Es el jugador al que se le espera la apuesta
		 *
		 * @return El valor de la apuesta si es que tuvo éxito; -1 en caso 
		 * contrario
		 */
		int recibir_apuesta(Jugador& ply);
		/**
		 * Envia una carta a traves de un socket hacia un host (jugador) unido 
		 * a la partida.
		 *
		 * @param card  Es la carta que se envía
		 * @param ply   Es el jugador al que se envía.
		 *
		 * @return 0 si se mandó correctamente y un valor negativo si no.
		 */
		int enviar_carta(Carta card, Jugador &ply);
		/**
		 * Devuelve un jugador que se haya conectado al servidor.
		 *
		 * @return un Jugador
		 */
		Jugador nuevo_jugador();
		/**
		 * Prepara toda la parte de conexion. Reserva un socket y setea la 
		 * direccion correctamente para que otros host se puedan conectar al 
		 * juego.
		 *
		 * @param puerto
		 */
		BlackJack(int puerto = 51717);
		/**
		 * Destructor. Cierra el socket abierto
		 */
		~BlackJack();
};

#endif
