#ifndef _JUGADOR_H_
#define _JUGADOR_H_

#include <vector>

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

#include "Carta.hpp"

enum class Comando
{
	PEDIR,     //!< pide una carta al crupier
	PLANTARSE, //!< se planta con las cartas que tiene
	SEGUIR,    //!< sigue jugando una nuevo ronda
	RETIRARSE,  //!< se retira del juego, cerrando la conexion
	NONE
};

class Jugador
{
	friend class BlackJack;

	private: 
		/**
		 * socket_fd se ocupa diferente dependiendo de donde se crea al 
		 * jugador. Si el jugador se crea en el servidor, entonces este socket 
		 * se ocupa para escribir y recibir mensajes hacia o desde el jugador.  
		 * Si el jugador se crea en el host cliente, entonces el socket se 
		 * ocupa para enviar y recibir mensajes hacia o desde el servidor.
		 */
		int socket_fd;
		struct sockaddr_in socket_dir;
		struct hostent *server;
		socklen_t largo;

		unsigned int capital; //!< dinero que le queda disponible
		unsigned int bet;     //!< apuesta actual
		std::vector<Carta> hand;

		/**
		 * Constructor desde servidor. Este constructor se emplea cuando se 
		 * espera que un nuevo jugador se una a la partida, luego que se haya 
		 * creado previamente con el constructor BlackJack::BlackJack(). Para 
		 * que el constructor termine su ejecucion es necesario que se llame 
		 * tambien a la funcion unirse_partida() desde algun host cliente.
		 *
		 * NOTA: Esta constructor solo se deberia usar a traves del metodo 
		 * BlackJack::nuevo_jugador()
		 *
		 * @param socketfd  Es el socket que reservó el servidor para recibir 
		 * conexiones externas.
		 */
		Jugador(int socketfd);
		
	public:
		/**
		 * Recibe el pago desde el servidor dependiendo de la apuesta hecha.  
		 * Esta funcion actualiza el capital del jugador, y elimina las cartas 
		 * de la mano.
		 *
		 * @return 0 si leyó correctamente y un valor negativo en otro caso.
		 */
		int recibir_pago();
		/**
		 * Devuelve el descriptor del socket usado para comunicarse con el 
		 * servidor.
		 *
		 * @return El "file descriptor" del socket.
		 */
		int sockfd();
		/**
		 * Dice el saldo que le queda disponible al jugador.
		 *
		 * @return El saldo que le queda
		 */
		unsigned int saldo();
		/**
		 * Borra las cartas de la mano actual, y setea la apuesta en 0.
		 */
		void nuevo_juego();
		/**
		 * Envía la desicion al crupier luego de recibir sus 2 primeras cartas.
		 *
		 * @param o Es el comando que se le envía al crupier, como Pedir o 
		 * Plantarse. Mas adelante se agregaran los comandos de apuestas: 
		 * Asegurar, Dividir, etc.
		 *
		 * @return 0 si la orden fué enviada con éxito y un valor negativo en 
		 * otro caso
		 */
		int decidir(Comando o);
		/**
		 * Dice el valor de la mano que mas le convenga al jugador.
		 *
		 * @return El valor de la mano.
		 */
		int valor_mano();
		/**
		 * Devuelve un string que contiene la representacion de todas las 
		 * cartas de su 'mano' actual.
		 * @return Un string con la concatenacion de todos los tags de las 
		 * cartas de su mano.
		 */
		std::string mano();
		/**
		 * Envia una apuesta al servidor.
		 *
		 * @param bet monto de la apuesta
		 *
		 * @return 0 si se envió correctamente
		 */
		int apostar(unsigned int amount);
		/**
		 * Recibe una carta desde el servidor.
		 * @return 0 si la recibió correctamente y un valor negativo si no
		 */
		int recibir_carta();
		/**
		 * Se une a un juego creado previamente. Esto le permite al jugador 
		 * recibir cartas, mandar apuestas; comunicarse con el servidor en 
		 * general
		 *
		 * @param puerto   Numero del puerto al que conectarse. 51717 por 
		 * defecto
		 * @param hostname Nombre del host al cual conectarse. "127.0.0.1" 
		 * (localhost) por defecto
		 * @return 0 si se pudo unir, y algun valor negativo en caso contrario
		 */
		int unirse_partida(int puerto = 51717, std::string hostname = "localhost");
		/**
		 * Destructor. Cierra los sockets abiertos.
		 */
		~Jugador();
		/**
		 * Constructor desde cliente. Este es el constructor por defecto. Para 
		 * poder comunicarse con el servidor es necesario llamar a la funcion 
		 * unirse_partida().
		 */
		Jugador();

};

/**
 * Pide la apuesta del jugador con un mensaje formateado.
 *
 * @param ply Jugador al que se le pide la apuesta
 *
 * @return La apuesta del jugador
 */
unsigned int pedir_apuesta(Jugador& ply);

/**
 * Pide la decision al jugador luego de que se le entreguen sus 2 primeras 
 * cartas.
 *
 * @param ply es el jugador que debe responder
 *
 * @return 
 */
Comando pedir_decision(Jugador& ply);

#endif
