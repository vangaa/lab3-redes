#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <cstring>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "error_output.hpp"
#include "Jugador.hpp"
#include "Carta.hpp"
#include <vector>
#include <algorithm>

const char sep[] = "\n\e[37;40m♣|♥|♠|♦|♣|♥|♠|  BLACKJACK  |♠|♦|♣|♥|♠|♦|♣|\e[0m"; 

int main(int argc, char* argv[])
{
	try
	{
		Jugador player;

		if (player.unirse_partida() < 0) error(EXIT_FAILURE, "Error al unirse a partida");

		// Aqui empieza el juego propiamente tal

		int apuesta = 1;
		std::vector<Carta> crup_hand; //!< mano del crupier
		int *scard = (int*) malloc(2*sizeof(int));
		unsigned int count;
		Comando c;
		bool blackjack = false;

		while (player.saldo() >= 10)
		{ // apuesta minima de 10
			std::cout << sep << std::endl;

			// se realiza la apuesta
			apuesta = pedir_apuesta(player);

			player.apostar(apuesta);

			if (apuesta == 0) break; //!< el jugador ya no quiere seguir jugando

			// recibo 2 cartas del crupier
			for (int i = 0; i< 2; i++)
			{
				count = read(player.sockfd(), scard, 2*sizeof(int));

				if (count == 2*sizeof(int))
					crup_hand.push_back(Carta::unserialize(scard));
			}

			// recibo las 2 cartas para mí

			for (int i = 0; i< 2; i++) player.recibir_carta();

			if (player.valor_mano() == 21) blackjack = true;

			std::cout << "\nMano del crupier: " << Carta::nn_tag() << " " << crup_hand[1].tag() << std::endl; 
			std::cout << "Mano del jugador: " << player.mano() << std::endl; 

			c = Comando::PEDIR;

			while (c != Comando::PLANTARSE && player.valor_mano() < 21)
			{ // Pedir cartas al crupier
				c = pedir_decision(player);

				player.decidir(c);

				if (c == Comando::PEDIR)
				{
					player.recibir_carta();
				} 
			}

			while (Carta::valor_mano(crup_hand) < 17 && player.valor_mano() < 22 && !blackjack)
			{ // Recibir las cartas del crupier hasta conseguir 17 puntos
				count = read(player.sockfd(), scard, 2*sizeof(int));

				if (count == 2*sizeof(int))
					crup_hand.push_back(Carta::unserialize(scard)); 
			}

			// Toma de decision del ganador
			std::cout << "\nCrupier (" << Carta::valor_mano(crup_hand) << "): " << Carta::manostr(crup_hand) << std::endl;
			std::cout << "Tu mano ("<< player.valor_mano() << "): " << player.mano() << std::endl;

			// recibir pago
			player.recibir_pago();
			crup_hand.clear(); // limpio la mano del crupier
			blackjack = false;

		}

	}
	catch (std::runtime_error& e)
	{
		error(EXIT_FAILURE, e.what());
	}

	exit(EXIT_SUCCESS);
}
