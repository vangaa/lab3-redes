#include "Carta.hpp"
#include <iostream>
#include <sstream>


Carta::Carta(int num, Pinta pinta)
{
	this->numero = num;
	this->pinta = pinta;

	std::ostringstream temp;


	switch (num)
	{
		case 1:
			temp << "A";
			break;
		case 11:
			temp << "J";
			break;
		case 12:
			temp << "Q";
			break;
		case 13:
			temp << "K";
			break;
		default:
			temp << num;
	} 

	switch (pinta)
	{
		case Pinta::CORAZON:
			temp << "♥";
			break;
		case Pinta::PICA:
			temp << "♠";
			break;
		case Pinta::DIAMANTE:
			temp << "♦";
			break;
		case Pinta::TREBOL:
			temp << "♣";
			break;
	}


	if (pinta == Pinta::PICA || pinta == Pinta::TREBOL)
		my_tag = "\e[5;30;47m" + temp.str() + "\e[0m"; 
	else
		my_tag = "\e[5;31;47m" + temp.str() + "\e[0m"; 

}

Carta::Carta()
{
	my_tag = "[**]";
	numero = 0;
}

std::string Carta::tag()
{
	return my_tag;
}

std::string Carta::nn_tag()
{
	return "\e[5;30;47m#\e[0m";
}

void * Carta::serialize()
{
	void *serobj = malloc(2 * sizeof(int));
	int *data = (int*) serobj;

	if (serobj != NULL)
	{
		data[0] = numero;
		data[1] = (int) pinta; //!<  mismo tamanio
	}
	return serobj;
}

Carta Carta::unserialize(void *serobj)
{
	int *data = (int*) serobj;

	Pinta pnt = (Pinta) data[1];

	return Carta(data[0], pnt);
}

int Carta::valor_mano(std::vector<Carta>& mano)
{
	int as_count = 0;
	int valor = 0;
	int diff;	//!< diferencia para llegar a 21

	std::vector<Carta>::iterator it;
	for (it = mano.begin(); it != mano.end(); ++it)
	{
		int num = (*it).numero;

		if (num > 1 && num < 10) valor += num;
		else if (num == 1) as_count++;
		else valor += 10;
	} 

	diff = 21 - valor;

	if (as_count > 0 && (11 + as_count-1) <= diff) valor += 11 + as_count - 1;

	else valor += as_count; 

	return valor;
}

std::string Carta::manostr(std::vector<Carta>& mano)
{
	std::vector<Carta>::iterator it;
	std::string str;

	for (it = mano.begin(); it != mano.end(); ++it)
	{
		str += (*it).tag() + " ";
	}
	
	return str;
}
