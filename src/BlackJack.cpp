#include "BlackJack.hpp"

#include <stdexcept>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

#ifndef DEBUG
	#define DEBUG 1
#endif

BlackJack::BlackJack(int puerto)
{
	socket_fd = socket(AF_INET, SOCK_STREAM, 0);

	if (socket_fd < 0) throw std::runtime_error("No se pudo reservar el socket\n");

	socket_dir.sin_family = AF_INET;
	socket_dir.sin_addr.s_addr = INADDR_ANY;
	socket_dir.sin_port = htons(puerto);

	if (bind(socket_fd, (struct sockaddr *) &socket_dir, sizeof(socket_dir)) < 0)
	{
		throw std::runtime_error("Error de binding");
	}

	listen(socket_fd, 5);
}

Jugador BlackJack::nuevo_jugador()
{
	return Jugador(socket_fd);
}

BlackJack::~BlackJack()
{
	close(socket_fd);
}

int BlackJack::enviar_carta(Carta card, Jugador &ply)
{
	int * data = (int*) card.serialize();
	unsigned int count = write(ply.socket_fd, data, 2*sizeof(int));

	if (count < 0) perror("Error al escribir en el socket");

	free(data); 

	if (count < 2 * sizeof(int)) return -1;

#if DEBUG
	std::cout << "Se envia la carta " << card.tag() << std::endl;
#endif

	ply.hand.push_back(card);

	return 0; 
}

int BlackJack::recibir_apuesta(Jugador& ply)
{
	unsigned int apuesta;
	int count = read(ply.socket_fd, &apuesta, sizeof(int));

	if (count < (int) sizeof(int))
	{
#if DEBUG
		std::cout << "Se leyeron " << count << " bytes\n";
		perror("BlackJack::recibir_apuesta()");
#endif
		return -1;
	}

#if DEBUG
	std::cout << "Se recibe la apuesta " << apuesta << std::endl;
#endif
	ply.bet = apuesta;

	return apuesta;
}

Comando BlackJack::recibir_decision(Jugador& ply)
{
	Comando com;
	unsigned int count = read(ply.socket_fd, &com, sizeof(Comando));

	if (count < sizeof(Comando)) com = Comando::NONE;

#if DEBUG
	std::cout << "Se recibe la decision\n";
#endif

	return com;
}

int BlackJack::enviar_pago(float mult, Jugador& ply)
{
	int pago = ply.bet * mult;
	int count; 

	count = write(ply.socket_fd, &pago, sizeof(int));

#if DEBUG
	std::cout << "Se escribieron " << count << " bytes\n";
#endif

	if (count < (int) sizeof(int))
	{
#if DEBUG
		perror("Funcion BlackJack::enviar_pago()");
#endif
		return -1;
	}

	ply.hand.clear();
	ply.bet = 0;
	ply.capital += pago;

#if DEBUG
	std::cout << "Al jugador se le paga " << pago << std::endl;
#endif

	return 0;
}
