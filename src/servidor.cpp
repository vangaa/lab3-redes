#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

#include <stdexcept>

#include "error_output.hpp"
#include "BlackJack.hpp"
#include "Jugador.hpp"
#include "Shoe.hpp"


int main(int argc, char* argv[])
{
	try
	{ 
		BlackJack juego = BlackJack();

		std::cout << "Esperando a un jugador...\n";
		Jugador p1 = juego.nuevo_jugador();
		std::cout << "Jugador encontrado\n"; 

		Shoe zap(5);  // zapato con 5 barajas

		/*
		 * La teoría dice que revolverlas 7 veces es suficiente para que las 
		 * cartas queden completamente aleatorias 
		 * (https://www.youtube.com/watch?v=AxJubaijQbI)
		 */

		for (int i = 0; i< 7; i++) zap.shuffle();

		// Aqui empieza el juego propiamente tal

		unsigned int apuesta;
		std::vector<Carta> crup_hand;  //!< mano del crupier
		int *scard;
		unsigned int count;
		Comando c;
		bool blackjack = false;

		while(!zap.empty())
		{
			apuesta = juego.recibir_apuesta(p1);

			if (apuesta == 0) break; //!< el jugador ya no quiere seguir jugando

			// saco y mando las cartas del crupier
			for (int i = 0; i< 2; i++)
			{
				crup_hand.push_back(zap.sacar_carta());
				scard = (int*) crup_hand[i].serialize();
				count = write(p1.sockfd(), scard, 2*sizeof(int));

				free(scard);
			} 

			// mando 2 cartas al jugador

			for (int i = 0; i< 2; i++) juego.enviar_carta(zap.sacar_carta(), p1);

			// condiciones para que el jugador tenga blackjack
			if ((p1.valor_mano() == 21 && crup_hand[1].numero < 10 && crup_hand[1].numero != 1)
					|| (p1.valor_mano() == 21 && Carta::valor_mano(crup_hand) < 21)
					)
				blackjack = true;

			c = Comando::PEDIR;

			while(c != Comando::PLANTARSE && p1.valor_mano() < 21)
			{ // mandar cartas al jugador; mientras las pida
				c = BlackJack::recibir_decision(p1);

				if (c == Comando::PEDIR)
				{
					juego.enviar_carta(zap.sacar_carta(), p1);
				}
			}

			while(Carta::valor_mano(crup_hand) < 17 && p1.valor_mano() < 22 && !blackjack)
			{ // mandar las cartas del crupier hasta alcanzar los 17 puntos

				crup_hand.push_back(zap.sacar_carta());
				scard = (int*) crup_hand.back().serialize();
				count = write(p1.sockfd(), scard, 2*sizeof(int));

				free(scard);
			}

			// enviar el pago al jugador

			int pscore = p1.valor_mano();
			int cscore = Carta::valor_mano(crup_hand);

			crup_hand.clear(); // se limpia la mano del crupier

			if (blackjack)
			{
				blackjack = false;
				juego.enviar_pago(2.5, p1);
			}

			else if (pscore < 22 && (pscore > cscore || cscore > 21))
				juego.enviar_pago(2, p1);

			else if (cscore < 22 && (pscore < cscore || pscore > 21))
				juego.enviar_pago(0, p1);

			else if (pscore < 22 && pscore == cscore)
				juego.enviar_pago(1, p1); 
		}

	}
	catch (std::runtime_error& e)
	{
		error(EXIT_FAILURE, e.what());
	} 

	exit(EXIT_SUCCESS);
}
