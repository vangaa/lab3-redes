#ifndef _CARTA_H_
#define _CARTA_H_

#include <string>
#include <vector>

enum class Pinta { PICA, DIAMANTE, CORAZON, TREBOL};

class Carta
{
	private:
		std::string my_tag;   //!< Como se muestra la carta

	public:
		Pinta pinta;
		int numero;   //!< el As se toma como 1, J como 11 y así...

		/**
		 * Devuelve un string que representa una lista de cartas.
		 *
		 * @param mano es la lista de cartas que se quiere mostrar como string
		 *
		 * @return un string con los tags de las cartas concatenados en orden
		 */
		static std::string manostr(std::vector<Carta>& mano);

		/**
		 * Devuelve el valor mas favorable (en el BlackJack) para una mano en 
		 * particular.
		 *
		 * @param mano Lista de cartas a la que se quiere calcular el valor
		 *
		 * @return El valor de la "mano"
		 */
		static int valor_mano(std::vector<Carta>& mano);

		/**
		 * Convierte una carta serializada en una instancia de tipo carta.
		 *
		 * @param serobj Direccion de memoria en donde se guarda la carta 
		 * serializada
		 *
		 * @return Un objeto de tipo carta.
		 */
		static Carta unserialize(void *serobj);
		/**
		 * Devuelve un espacio de memoria de 8B, los cuales contienen la 
		 * informacion necesario para reconstruir una carta al otro lado de la 
		 * comunicacion.
		 *
		 * @return Un puntero al objeto serializado.
		 */
		void * serialize();
		/**
		 * Devuelve el tag de una carta oculta.
		 *
		 * @return el string X, con color de fondo gris
		 */
		static std::string nn_tag();
		/**
		 * Constructor por defecto. Tag: [**]
		 */
		Carta(); 
		/**
		 * Constructor.
		 *
		 * @param num   Numero de la carta.
		 * @param pinta Pinta de la carta
		 */
		Carta(int num, Pinta pinta);
		/**
		 * Devuelve el tag de la carta. El tag es la representacion de la carta 
		 * como un string.
		 *   Ej. Si la carta es As de diamantes, el tag es: [A♦]
		 *
		 * @return un string en representacion de la carta
		 */
		std::string tag();

};

#endif
