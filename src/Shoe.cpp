#include "Shoe.hpp"
#include <algorithm>
#include <cstdlib>


Shoe::Shoe(int num_b)
{
	for (int i = 0; i < num_b; i++)
	{ // se crean las cartas segun numero de barajas
		for (int j = 1; j <= 13; j++)
		{ // creacion de las picas
			Carta aux(j, Pinta::PICA);
			mazo.push_back(aux);
		}
		for (int j = 1; j <= 13; j++)
		{ // creacion de los diamantes
			Carta aux(j, Pinta::DIAMANTE);
			mazo.push_back(aux);
		}
		for (int j = 1; j <= 13; j++)
		{ // creacion de los treboles
			Carta aux(j, Pinta::TREBOL);
			mazo.push_back(aux);
		}
		for (int j = 1; j <= 13; j++)
		{ // creacion de los corazones
			Carta aux(j, Pinta::CORAZON);
			mazo.push_back(aux);
		}
	}

}

static int random_gen(int i)
{
	std::srand(time(NULL));
	return std::rand()%i;
}

void Shoe::shuffle()
{
	std::random_shuffle(mazo.begin(), mazo.end(), random_gen);
}

int Shoe::num_cartas()
{
	return mazo.size();
}

Carta Shoe::sacar_carta()
{
	Carta aux = mazo.back();

	mazo.pop_back();

	return aux;
}

bool Shoe::empty()
{
	return mazo.empty();
}
