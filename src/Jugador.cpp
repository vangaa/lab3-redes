#include "Jugador.hpp"
#include <string>
#include <cstring>
#include <stdexcept>


Jugador::Jugador(int socketfd)
{
	socket_fd = accept(socketfd,
			(struct sockaddr *) &socket_dir,
			&largo);

	if (socket_fd < 0) throw std::runtime_error("Error al aceptar la conexion");
}

Jugador::Jugador() : socket_fd(0)
{
	capital = 100;
	bet = 0;

	socket_fd = socket(AF_INET, SOCK_STREAM, 0);

	if (socket_fd < 0) throw std::runtime_error("Error al reservar socket"); 

	memset(&socket_dir, 0, sizeof(socket_dir));

	socket_dir.sin_family = AF_INET;
}

Jugador::~Jugador()
{
	close(socket_fd);
}

int Jugador::unirse_partida(int puerto, std::string hostname)
{
	server = gethostbyname(hostname.c_str());

	if (server == NULL) return -1;

	memcpy(&socket_dir.sin_addr.s_addr,
			server->h_addr,
			server->h_length);

	socket_dir.sin_port = htons(puerto);

	if (connect(socket_fd, (struct sockaddr *) &socket_dir, sizeof(socket_dir)) < 0)
	{
		return -2;
	}

	return 0;
}

int Jugador::recibir_carta()
{
	int *data = (int*) malloc(2 * sizeof(int));
	int count;

	if (data == NULL) return -2;

	count = read(socket_fd, data, 2*sizeof(int));

	if (count < (int) (2*sizeof(int)))
	{
#if DEBUG
		std::cout << "Se leyeron " << count << " bytes\n";
		perror("Jugador::recibir_carta()");
#endif
		free(data);
		return -1;
	}

	Carta nueva = Carta::unserialize(data);
	free(data);

	hand.push_back(nueva);

	return 0;
}

int Jugador::apostar(unsigned int amount)
{
	if (amount % 10 != 0) return -2;
	if (amount > capital) return -3;

	unsigned int count = write(socket_fd, &amount, sizeof(int));

	if (count < sizeof(int))
	{
#if DEBUG
		std::cout << "Se escribieron " << count << " bytes\n";
		perror("Jugador::apostar()");
#endif 
		return -1;
	}

	capital -= amount;
	this->bet = amount;

	return 0;
}

int Jugador::valor_mano()
{
	return Carta::valor_mano(hand);
}

std::string Jugador::mano()
{
	return Carta::manostr(hand);
}

int Jugador::decidir(Comando o)
{
	unsigned int count = write(socket_fd, &o, sizeof(Comando));

	if (count < sizeof(Comando))
	{
#if DEBUG
		std::cout << "Se escribieron " << count << " bytes\n";
		perror("Jugador::decidir()");
#endif
		return -1;
	}

#if DEBUG
	std::cout << "Se decide\n";
#endif

	return 0;
}

void Jugador::nuevo_juego()
{
	hand.clear();
	bet = 0;
}

unsigned int Jugador::saldo()
{
	return capital;
}


unsigned int pedir_apuesta(Jugador& ply)
{
	unsigned int apuesta;
	std::string msg = "Cuanto desea apostar ? (multiplos de 10): ";
	bool again;

	do
	{
		again = false;
		std::cout << "\n\e[1;34mSaldo: " << ply.saldo() << "\e[0m\n";
		std::cout << "\e[1;34m" << msg << "\e[0m";
		std::cin >> apuesta; 

		if (apuesta % 10 != 0)
		{
			std::cout << "Debe ser multiplo de 10\n"; 
			again = true;
		}
		else if (apuesta > ply.saldo())
		{
			std::cout << "No dispone de ese dinero\n";
			again = true;
		}
	}
	while(again);

	return apuesta;
}

Comando pedir_decision(Jugador& ply)
{
	Comando com;
	std::string msg = "Que desea hacer [h,s,?] ?: ";
	std::string help = "h - pedir carta\n"
		"s - plantarse\n"
		"? - muestra la ayuda\n";

	std::string ans;

	do
	{
		std::cout << "\n\e[1;34mMano:\e[0m" << ply.mano() << std::endl;
		std::cout << "\e[1;34m" << msg << "\e[0m";

		std::cin >> ans;

		switch (ans[0])
		{
			case '?':
				std::cout << "\n\e[1;31m" << help << "\e[0m";
				com = Comando::NONE;
				break;
			case 'h':
				com = Comando::PEDIR;
				break;
			case 's':
				com = Comando::PLANTARSE;
				break;
			default:
				com = Comando::NONE; 
		} 
	}
	while(com == Comando::NONE);

	return com;
}

int Jugador::sockfd()
{
	return socket_fd;
}

int Jugador::recibir_pago()
{
	int pago;
	unsigned int count = read(socket_fd, &pago, sizeof(int));

	if (count < sizeof(int))
	{
#if DEBUG
		std::cout << "Se leyeron " << count << " bytes\n";
		perror("Jugador::recibir_pago()");
#endif
		return -1;
	}

	capital += pago;
	bet = 0;
	hand.clear();

	std::cout << "Usted ha ganado " << pago << std::endl;

	return 0;
}
