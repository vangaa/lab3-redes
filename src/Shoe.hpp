#ifndef _SHOE_H_
#define _SHOE_H_

#include <vector>

#include "Carta.hpp"

class Shoe
{
	private:
		std::vector<Carta> mazo;

	public:
		/**
		 * Dice si quedan cartas en el mazo
		 *
		 * @return true si quedan; false si no.
		 */
		bool empty();
		/**
		 * Constructor.
		 *
		 * @param num_b
		 */
		Shoe(int num_b);
		/**
		 * Devuelve la primera carta de el mazo, y la elimina posteriormente.
		 *
		 * @return 
		 */
		Carta sacar_carta();
		/**
		 * Dice el números de cartas que quedan en el mazo de cartas.
		 *
		 * @return El numero de cartas que quedan
		 */
		int num_cartas();

		/**
		 * "Revuelve" las cartas del mazo. Las ordena de forma random.
		 */
		void shuffle();
};

#endif
