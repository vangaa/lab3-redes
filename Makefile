CC = @g++
CFLAGS = -Wall -std=c++11

EXEC = cliente servidor
OBJ = Carta.o BlackJack.o Jugador.o error_output.o Shoe.o
OBJDIR = obj
VPATH = src/

.PHONY: all clean

all: cliente servidor

servidor: $(addprefix $(OBJDIR)/, servidor.o $(OBJ))
	@echo Creando ejecutable «$@»
	$(CC) $(CFLAGS) -o $@ $^ 

cliente: $(addprefix $(OBJDIR)/, cliente.o $(OBJ))
	@echo Creando ejecutable «$@»
	$(CC) $(CFLAGS) -o $@ $^ 

# Se crea el directorio de salida del codigo objeto
$(OBJDIR):
	@echo Creando directorio del codigo objeto.
	@mkdir -p $@

$(OBJDIR)/%.o: %.cpp | $(OBJDIR)
	@echo Compilando «$<»
	$(CC) $(CFLAGS) -o $@ -c $<

# dependencias
$(OBJDIR)/cliente.o: error_output.hpp Jugador.hpp Carta.hpp
$(OBJDIR)/error_output.o: error_output.hpp
$(OBJDIR)/servidor.o: Jugador.hpp error_output.hpp BlackJack.hpp Shoe.hpp
$(OBJDIR)/Carta.o: Carta.hpp
$(OBJDIR)/Jugador.o: Jugador.hpp Carta.hpp
$(OBJDIR)/BlackJack.o: BlackJack.hpp Jugador.hpp
$(OBJDIR)/Shoe.o: Shoe.hpp Carta.hpp


clean:
	@echo Limpiando...
	@rm -rf $(OBJDIR)
	@rm -f $(EXEC)
